.. pierre documentation master file, created by
   sphinx-quickstart2 on Mon May 28 03:25:55 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Pierre - A Framework for Optimal Control
========================================

Introduction
------------

Pierre is a framework for writing optimal controllers and evaluating these
through experiments.

**Features:**
    - Scheduled Controllers
    - IO Interface Generation
    - Signal Processing (incl. OPC)

Example: Obstacle Robot Control
-------------------------------

Controller:

.. sourcecode:: python

    class ORMController(pymdp.control.OnlineMDPController):
        def __init__(self,mdp_fname,io_interface):
            mdp = pymdp.MDP.readCassandra(mdp_fname)
            super(ORMController,self).__init__(mdp=mdp,io_interface=io_interface)

            self.current_state = None
            self.has_sensed = False

        @pierre.control.schedule(EveryNMilliSeconds(2000))
        def act(self):
            if self.has_sensed:
                print("Best action from state %i: %i"
                    % (self.current_state,self.best_action(self.current_state)))
                self.io_interface.act(self.best_action(self.current_state))

        @pierre.control.schedule(EveryNMilliSeconds(1000))
        def sense(self):
            self.current_state = self.io_interface.sense()
            print("Current states changed to: %i" % self.current_state)
            if not self.has_sensed:
                self.has_sensed = True

IO Interface:

.. sourcecode:: python

    class RobotIOInterface(IOInterface):

        signal_processor = SignalProcessorClient(identity='opc_signal_processor')

        pos_x = UnsignedIntegerSignal(alias='orm.pos_x',max=9,read_only=True)
        pos_y = UnsignedIntegerSignal(alias='orm.pos_y',max=19,read_only=True)

        moves = ('Stay','North','NorthEast','East','SouthEast','South',
                 'SouthWest','West','NorthWest')

        move  = StringSignal(alias='orm.move',start_value='Stay',choices=moves)

        def sense(self):
            pos_x = self.pos_x.value
            pos_y = self.pos_y.value
            return self.get_state_from_pos(pos_x,pos_y)

        def act(self,value):
            self.move.value = self.moves[value]

API Documentation
-----------------

.. toctree::
   :maxdepth: 4

   pierre


Indices and Tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

