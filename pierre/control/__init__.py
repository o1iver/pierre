# -*- coding: utf-8 -*-
"""
    pierre.control
    ~~~~~~~~~~~~~~


    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

import random

import pierre
import pierre.scheduling as scheduling

def schedule(event):
    """
    Decorator generator that attaches an event to a function.

    :param event: Event :py:class:`pierre.scheduling.Event`.
    :type event: :py:class:`pierre.scheduling.Event`
    :rtype: Decorator function.
    """
    def inner(func):
        setattr(func,'_s_event',event)
        return func
    return inner

class Controller(pierre.PierreObject):
    """
    Controller base class. A controller acts on the system based on sensor
    data it receives from the system. I/O between the real-world (or model)
    is handled by the controller's :py:class:`pierre.io.IOInterface` instance.

    Controllers should subclass this base class as it provides a scheduled
    control loop. Control logic should be defined in **parameter-less** methods,
    decorated using a decorator produced by the
    :py:func:`.schedule` decorator generator. All functions
    decorated like this will automatically be called at the scheduled time.

    The controller should finally be started simply by calling the
    :py:meth:`.Controller.control` method. Any pre-/post-control functionality
    can be placed in the :py:meth:`.Controller.pre_control` and
    :py:meth:`.Controller.post_control` methods; these are automatically called
    just before the control loop is entered and just after it exits. If the
    :py:meth:`.Controller.control` method is overridden, these
    post/pre methods must be called manually.

    .. note::

        Scheduled methods must be **parameter-less**, meaning that they should
        only take *self* as a parameter.

    **Example**: below is the source code for a simple controller. If only has a
    single method, which is scheduled to be triggered every 2 minutes.

     .. sourcecode:: python

        class SimpleController(Controller):

            @schedule(pierre.scheduling.EveryNMinutes(2))
            def print_status(self):
                now = time.strftime("%a, %d %b %Y %H:%M:%S")
                self.logger.info("controller status: good; time: %s" % now)


    :param io_interface: IO-interface responsible for mapping (real space
                         to/from controller space) and signaling.
    :type io_interface: :py:class:`pierre.io.IOInterface`

    :param scheduler: optional scheduler (if none is given, a new
                      :py:class:`pierre.scheduling.Scheduler` is created).
    :type scheduler: :py:class:`pierre.scheduling.Scheduler`


    .. todo::

        How do we order methods that are scheduled at the same intervals (see
        `<https://bitbucket.org/o1iver/pierre/issue/2/scheduler-event-ordering>`_)

     """

    def __init__(self,io_interface,scheduler=None):
        self.io_interface = io_interface

        self.scheduler = scheduler or scheduling.Scheduler()
        self._init_scheduled()

    def _init_scheduled(self):
        """
        This method attaches scheduled functions to the instance's scheduler
        by looping through the instance attributes and looking for any
        callables with a '_s_event' attribute (does not look at attributes
        starting with '__').
        """
        for n in filter(lambda x: not x.startswith('__'),dir(self)):
            attr = getattr(self,n)
            if hasattr(attr,'__call__') and hasattr(attr,'_s_event'):
                self.scheduler.attach(event=getattr(attr,'_s_event'),
                    callback=attr,args=(),kwargs={})

    def control(self):
        """
        This method should be called to start the control loop. This method does
        not need to be overridden; if it is overridden, make sure to call both
        the :py:meth:`pierre.control.Controller.pre_control` and the
        :py:meth:`pierre.control.Controller.post_control` methods manually.
        """
        self.pre_control()
        try:
            self.scheduler.loop()
        except KeyboardInterrupt:
            pass
        self.logger.info('Exciting control loop!')
        self.post_control()

    def pre_control(self):
        """
        Method that contains any logic required before the controller enters its
        control loop (empty in base class).
        """
        pass

    def post_control(self):
        """
        Method that contains any logic required after the control exits its
        control loop (empty in base class).
        """
        pass
