# -*- coding: utf-8 -*-
"""
    pierre.control.examples
    ~~~~~~~~~~~~~~~~~~~~~~~


    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

from pierre.control import Controller
from pierre.control import schedule

from pierre.scheduling import EveryNSeconds


class RobotController(Controller):
    """
    Example robot controller.

    :param io_interface: IO-interface responsible for mapping (real space
                         to/from controller space) and signaling.
    :type io_interface: :py:class:`pierre.io.IOInterface`

    :param scheduler: optional scheduler (if none is given, a new
                      :py:class:`pierre.scheduling.Scheduler` is created).
    :type scheduler: :py:class:`pierre.scheduling.Scheduler`

    """

    def __init__(self,io_interface,scheduler=None):
        super(RobotController,self).__init__(io_interface,scheduler)
        self.last_move = 0

    @schedule(EveryNSeconds(4))
    def sense(self):
        """
        The controller's sense method.
        """
        self.last_move = self.io_interface.sense()
        self.logger.debug('Controller sensed: %i' % self.last_move)

    @schedule(EveryNSeconds(4))
    def act(self):
        """
        The controller's act method.
        """
        next_move = self.last_move + 1 if self.last_move < 8 else 0
        self.io_interface.act(next_move)
        self.logger.debug('Controller acted:  %i' % next_move)


