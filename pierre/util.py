# -*- coding: utf-8 -*-
"""
    pierre.util
    ~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

def initialize(obj,**kwargs):
    """
    Initializes an object with the keyword arguments provided in the call.
    """
    for k,v in kwargs.items():
        obj.__dict__[k] = v

def numInIter(iter_,obj):
    """
    Returns the number of times an object is in an iterator object by comparing
    the items' IDs with the object's ID.

    :param iter_: iterator object.
    :param obj: object
    :rtype: int
    """
    fpred = lambda x: id(x) == id(obj)
    return len(filter(fpred,iter_))

def inIter(iter_,obj):
    """
    Checks whether or not is returned by an iterator by comparing the IDs of
    the returned objects with the ID of the object.

    :param iter_: iterator object.
    :param obj: object
    :rtype: bool
    """
    return not (numInIter(iter_,obj) == 0)

def callCount(method):
    """
    Instance method decorator that counts the number of times a function is
    called. It stores the counter for every registered method in a dictionary
    in the object's dict at 'funcCallCount'. The call count only occurs if the
    instance or it's class has an attribute 'DEBUG' set to True.

    :param method: instance method.
    :rtype: decorated instance method.
    """
    def inner(obj,*args,**kwargs):
        key = str(method)
        try:
            debug = getattr(obj,"DEBUG")
        except AttributeError:
            debug = False
        if debug:
            try:
                obj.__dict__['_CC_'][key] += 1
            except KeyError:
                try:
                    obj.__dict__['_CC_'][key] = 1
                except KeyError:
                    obj.__dict__['_CC_'] = {}
                    obj.__dict__['_CC_'][key] = 1
        res = method(obj,*args,**kwargs)
        return res
    return inner

def toList(data):
    """
    Function that converts a list, set, map or tuple to a new list, a single
    value to a singleton list and anything else to an empty list.

    :param data: data
    :rtype: list
    """
    if type(data) in (list,set,map,tuple):
        return list(data)
    if data:
        return [data]
    return []

def map_(funcs,arg):
    """
    Maps different functions over the same argument.

    :param funcs: functions to map over the argument.
    :param arg: argument to call functions with.
    """
    return [func(arg) for func in funcs]

def convert_to_default_dict(dict_,def_=lambda: None):
    d = collections.defaultdict(def_)
    for k,v in dict_.items():
        d[k] = v
    return d

def add_none_elems(dict_,attrs):
    missing = [k for k in attrs if k not in dict_.keys()]
    for m in missing:
        dict_[m] = None
    return dict_

def create_null_array(null,*args):
    if not all(map(lambda x: type(x) == int,args)):
        raise ValueError("Dimensions must be integers!")
    if filter(lambda x: x < 1,args):
        raise ValueError("Dimensions must be greater or equal to 1!")
    l = []
    if len(args) == 1:
        l = [null]*int(args[0])
    else:
        for n in range(args[0]): l.append(create_null_array(null,*args[1:]))
    return l

def not_func(func):
    def inner(*args,**kwargs):
        return not func(*args,**kwargs)
    return inner

def arg_max(iter_):
    return arg_max_(iter_)[0]

def arg_max_(iter_):
    n = -1
    v = float('-inf')
    for n_,v_ in enumerate(iter_):
        if v_ > v:
            n = n_
            v = v_

    return n,v


