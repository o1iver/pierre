# -*- coding: utf-8 -*-
"""
    pierre.errors
    ~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

class PierreError(Exception):
    pass

class PierreIOError(PierreError,IOError):
    pass
