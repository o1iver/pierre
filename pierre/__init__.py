# -*- coding: utf-8 -*-
"""
    pierre
    ~~~~~~

    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

import os
import sys

import logging
import logging.handlers

# Get log handler type from env var.
DEFAULT_LOGGER = 'stdout'
LOGGER_TYPE = os.getenv('PIERRE_LOG_TYPE',DEFAULT_LOGGER)

LOGGER_TYPES = ['stdout','stderr','syslog']

if LOGGER_TYPE not in LOGGER_TYPES:
    print('ERROR: invalid logger type \'%s\'' % LOGGER_TYPE)
    sys.exit(1)

# Get log level from env var.
DEFAULT_LOGGER_LEVEL = 'info'
LOGGER_LEVEL = os.getenv('PIERRE_LOG_LEVEL',DEFAULT_LOGGER_LEVEL)

LOG_LEVELS = {'debug': logging.DEBUG,
               'info': logging.INFO,
               'warning': logging.WARNING,
               'error': logging.ERROR,
               'critical': logging.CRITICAL}

if LOGGER_LEVEL not in LOG_LEVELS:
    print('ERROR: invalid log level \'%s\'' % LOGGER_LEVEL)
    sys.exit(1)

LOGGER_LEVEL = LOG_LEVELS[LOGGER_LEVEL]

class PierreObject(object):
    """
    Pierre base class. Any class that represents a complex entity should
    subclass this base class as it provides a thread-/process-safe logger (via
    syslog).
    """

    # The log format defines the way that logged messaged are displayed by the
    # logging handler. The current format will result in the following sequence:
    #   1. asctime,
    #   2. log level name,
    #   3. id of the logger's owner object,
    #   4. process ID (if available),
    #   5. log message.
    LOG_FORMAT = "%(asctime)s [%(levelname)s] [{cls}] " +\
                 "[%(process)d]: %(message)s"

    # Defines the log level for all Pierre objects.
    LOG_LEVEL  = LOGGER_LEVEL

    def __repr__(self):
        if not hasattr(self,"__unicode__"):
            return u"%s:%s" % (self.__class__,id(self))
        else:
            unicode_ = self.__unicode__() + u"@" +  unicode(id(self))
            return unicode_.encode('utf-8')

    @property
    def logger(self):
        """
        Every :py:class:`.PierreObject` has access to an internal logger. Logging is
        capsuled within the objects themselves because of concurrency issues.
        The current logging configuration logs to syslog (also because of
        concurrency issues with using stdin/stdout/stderr.
        """
        if not hasattr(self,"_logger"):
            self._logger = logging.getLogger(str(id(self)))

            # Log level
            self._logger.setLevel(PierreObject.LOG_LEVEL)

            # Log handler
            # STDERR
            if  LOGGER_TYPE == 'stderr':
                log_handler = logging.StreamHandler(sys.stderr)
            # SYSLOG
            elif LOGGER_TYPE == 'syslog':
                log_handler = logging.handlers.SysLogHandler(address="/dev/log")
            # Default is STDOUT
            else:
                log_handler = logging.StreamHandler(sys.stdout)


            # Handler format
            log_formatter = logging.Formatter(PierreObject.LOG_FORMAT.format(cls=self.__class__.__name__))
            log_handler.setFormatter(log_formatter)

            self._logger.addHandler(log_handler)

        return self._logger







