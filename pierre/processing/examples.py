# -*- coding: utf-8 -*-
"""
    pierre.processing.examples
    ~~~~~~~~~~~~~~~~~~~~~~~~~~


    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""
import time
from pierre.processing import *

class ErrorService(Service):
    def __init__(self, identity, err_secs, services_addr=None, timeout=None):
        super(ErrorService, self).__init__(identity, services_addr, timeout)

        self.err_secs = err_secs
        self.now = time.time()

    def handle(self, client, signal, data):
        elapsed = time.time() - self.now
        if elapsed >= self.err_secs:
            raise RuntimeError('On purpose error!')

        reply = 'Still have %f seconds left until error!'\
        % (self.err_secs - elapsed)
        self.broker.send_multipart([client, S_SUCCESS, reply])
