# -*- coding: utf-8 -*-
"""
    pierre.processing
    ~~~~~~~~~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.

    This module provides a service-oriented processing API. It defines a
    :py:class:`.Broker` type, a :py:class:`Service` type and a
    :py:class:`.Client` type. The :py:class:`.Broker` can be used as-is, but
    custom *service* and *clients* should be implemented depending on the
    use-case.

    In the next few paragraphs, the general architecture and the messaging
    protocol are be described.

    Architecture:
    -------------

    This SOA library has three main compontents, *brokers*, *services* and
    *clients*.
    
    *Brokers* (generally there is only a single *broker* running),
    are responsible for routing requests between *clients*/*services*, providing
    a *service* registration interface, checking if *services* are still alive,
    and finally offering a *service* unregistration interface.

    *Services* are long-running processes that receive requests (routed from
    *clients* via the *broker*, or sent directly by the *broker*). The *services*
    may or may not reply to the request sender by routing a response message
    through the *broker*.

    *Clients* are attached to more interesting ojects, providing them with a
    simple *service request* API (see: :py:meth:`.Client.request`).

    The socket architecture is relatively simple: the *broker* binds to two
    addresses (using sockets of type :py:data:`zmq.ROUTER`), the *client
    interface* on one side and the *service interface* on the other. *Clients*
    connect to the *client interface* (using a socket of type
    :py:data:`zmq.DEALER`)and *services* connect to the *service interface*
    (using a socket of type :py:data:`zmq.DEALER`). *Clients* and *services*
    never communicate directly.

    When *services* intially connect to the *service interface*, they register
    themselves with the *broker*. This allows the broker to refuse client 
    requests to services that are not registered. This is necessary, because
    routed requests would otherwise never arrive (and the client would never
    know, at least not easily). When *services* shut down, they must also
    let the *broker* know that they are no longer serving and requests to them
    should no longer be routed.

    In order to safe-guard against dead/stuck *services*, the *broker*
    periodically performs heartbeat checks. It does this by sending the service
    a request that it must reply to within a certain amount of time. If the
    *service* does not reply within the required time range, it will be removed
    from the list of registered services, meaning that *clients* will no longer
    be able to communicate with it.

    .. note::

        The heartbeat timeout time may have to be adjusted for services that
        block for a long time (ex: planners).

    Messaging Protocol:
    -------------------

    The messaging protocol is very simple. There are four type of messages
    depending on sender and recipient type. *Note that this is the messaging
    protocol that should be used by API consumers; internally the protocol
    modifies the messages slightly for routing reasons.*

    .. note::
        
        The message formats below are always lists of frames, meaning that they
        should be sent using the :py:meth:`zmq.Socket.send_multipart` method.

    Client -> Service:
    ``````````````````

    Requests from *client* to *services* have the following format:

    .. sourcecode:: none

        [service_ident,service_signal,data]

    Service -> Client:
    ``````````````````

    Replies to requests that a *service* sends to a client has the following
    format:
    
    .. sourcecode:: none

        [client_ident,service_signal,data]

    Broker -> Service:
    ``````````````````

    Requests from the *broker* to *services* (such as a *shutdown* request)
    have the following format:

    .. sourcecode:: none

        [service_signal]

    Service -> Broker:
    ``````````````````

    *Service* replies to the broker have the following format:

    .. sourcecode:: none

        [service_signal]
"""

import json
import time
import zmq
import signal as signal_

from binascii import hexlify

import pierre
import pierre.errors

class IPCReadTimeout(pierre.errors.PierreIOError):
    """
    Exception used to represent an IPC read timeout. It is raised when an SOA
    entity's read fails because the polling timeout was reached before a message
    was received.
    """
    pass


#: Signal used by the service so signal to the broker that the
#: service is ready to serve.
#: :data: none
S_REGISTER = 'S_REGISTER'

#: Signal used by the service to signal to the broker that the
#: service is shutting down and no longer able to serve.
#: :data: none
S_UNREGISTER = 'S_UNREGISTER'

#: Signal used by the broker when responding to a service
#: request that could not be fulfilled because the requested
#: service is not registered.
#: :data: none
S_UNAVAILABLE = 'S_UNAVAILABLE'

#: Signal used by the broker to signal that the service could
#: not be registered with the given identifier, because another
#: service is already registered with the given identifier.
S_IDENTTAKEN = 'S_IDENTTAKEN'

#: Signal used by the broker to check for a service's
#: heartbeat. Upon receipt of such a signal, a service must
#: immediately reply with a S_SUCCESS to signal that it is
#: still alive and well.
S_HEARTBEAT = 'S_HEARTBEAT'

#: Signal used by the client to signal that the service must
#: reply to the request it has received.
#: :data: allowed
S_MUSTREPLY = 'S_MUSTREPLY'

#: Signal used by the client to signal that the service should
#: no reply to a request.
#: :data: allowed
S_NOTREPLY = 'S_NOTREPLY'

#: Signal used by a service to signal a successful request
#: completion (to either a client or the broker).
#: :data: allowed
S_SUCCESS = 'S_SUCCESS'

#: Signal used by a service to signal an unsuccesful request
#: completion (to either a client or the broker).
#: :data: failure message (if available)
S_FAILURE = 'S_FAILURE'

#: Signal used by a client or the broker to signal to a service
#: that it should shut down.
#: :data: none
S_SHUTDOWN = 'S_SHUTDOWN'

#: List containing all permitted signals (used to check for valid signals).
SIGNALS = [S_REGISTER, S_UNREGISTER, S_UNAVAILABLE, S_IDENTTAKEN, S_HEARTBEAT,
           S_MUSTREPLY, S_NOTREPLY, S_SUCCESS, S_FAILURE, S_SHUTDOWN]

DEFAULT_TIMEOUT = 50
DEFAULT_HEARTBEAT = 20

DEFAULT_CLIENTS_ADDR = 'tcp://127.0.0.1:8888'
DEFAULT_SERVICES_ADDR = 'tcp://127.0.0.1:9999'


class Broker(pierre.PierreObject):
    """
    The broker routes requests and replies between clients and services, handles
    service registration and makes heartbeat-checks with registered services.

    :param services_addr: String with the services socket interface. All
                          services must connect to this address to register with
                          the broker and to receive requests and potentially
                          send replies. If this parameter is not set or None,
                          the module-level DEFAULT_SERVICES_ADDR address is used.

    :param clients addr: String with the client socket interface address. All
                         clients must connect to this socket to send and receive
                         message from/to services. If this parameter is not set
                         or None, the module-level DEFAULT_CLIENTS_ADDR address
                         is used.

    :param heartbeat_time: Number of seconds, as int, between heartbeat checks
                           are done for all services. If this parameter is not
                           passed or None, the module-level DEFAULT_HEARTBEAT
                           value is used.

    :param timeout: Polling timeout in milliseconds as an int or float. If not
                    set or None, the module-level DEFAULT_TIMEOUT is used.
    """

    def __init__(self, services_addr=None, clients_addr=None,
                 heartbeat_time=None,
                 timeout=None):

#        def _print(str_):
#            print(str_)
#
#        self.logger.debug = _print
#        self.logger.info = _print
#        self.logger.warning = _print
#        self.logger.error = _print

        self.services_addr = services_addr or DEFAULT_SERVICES_ADDR
        self.clients_addr = clients_addr or DEFAULT_CLIENTS_ADDR
        self.heartbeat_time = heartbeat_time or DEFAULT_HEARTBEAT
        self.timeout = timeout or DEFAULT_TIMEOUT

        self.registered_services = []
        self.outstanding_heartbeats = []

        self.context = zmq.Context()
        self.poller = zmq.Poller()

        self.services = self.context.socket(zmq.ROUTER)
        self.clients = self.context.socket(zmq.ROUTER)

        self.poller.register(self.services, zmq.POLLIN)
        self.poller.register(self.clients, zmq.POLLIN)

        self.services.bind(self.services_addr)
        self.clients.bind(self.clients_addr)

        time.sleep(1)

        self.logger.info('Broker initializing...')


    def serve(self):
        """
        Start the broker's routing service. After this method is called, the
        broker enters an infinite loop (until a :py:exc:`KeyboardInterrupt` is
        received), where it routes requests, handles service registration and
        unregistration and performs heartbeat checks on the registered services.
        """

        def sigint(sig,frame):
            self.running = False
            self.logger.info('Received signal %i. Quitting!' % sig)

        signal_.signal(signal_.SIGINT,sigint)
        signal_.signal(signal_.SIGTERM,sigint)

        self.logger.info('Entering service loop...')

        self.running = True
        self.last_heartbeat = time.time()
        while self.running:
            # See if heartbeat checks due
            now = time.time()
            if int(now - self.last_heartbeat) >= self.heartbeat_time:
                for service in self.outstanding_heartbeats:
                    self.logger.warning('Service %s has not responded to '\
                                        'heartbeat check. Unregistering...'
                                            % service)

                    self.outstanding_heartbeats.remove(service)

                    try: self.registered_services.remove(service)
                    except ValueError: pass

                self.logger.debug('Checking services\' heartbeats.')
                if self.registered_services:
                    self.logger.debug('Currently registered service:')
                else:
                    self.logger.debug('No registered services.')

                for service in self.registered_services:
                    self.logger.debug(' - %s' % service)

                self._send_heartbeat_checks()
                self.last_heartbeat = now

            try:
                incoming = dict(self.poller.poll(self.timeout))
            except KeyboardInterrupt:
                raise
                #self.logger.info('Broker shutting down.')
                #self.running = False
                #break
            except zmq.ZMQError:
                pass

            # Service message
            if self.services in incoming and incoming[
                                             self.services] == zmq.POLLIN:
                msg = self.services.recv_multipart()
                self.logger.debug('Received service msg: %s' % str(msg))

                assert len(msg) in (2, 4)

                if len(msg) == 2:
                    service, signal = msg
                    # Registration signal
                    if   signal == S_REGISTER:
                        if service not in self.registered_services:
                            self.registered_services.append(service)
                            self.logger.debug(
                                'Service %s registering.' % service)
                            self.services.send_multipart([service, S_SUCCESS])
                        else:
                            self.logger.debug(
                                'Service tried to register with ' +
                                'ident of already registered service %s.'
                                % service)
                            #TODO: this doesnt work! the S_IDENTTAKEN never arrives.
                            #      This should work because usually if a socket with
                            #      an identical ident connects to a router, the newer
                            #      socket it first conntacted and when that leaves,
                            #      the other socket is used. Strangely enough it
                            #      still doesnt work here.
                            self.services.send_multipart(
                                [service, S_IDENTTAKEN])

                    # Unregistration signal
                    elif signal == S_UNREGISTER:
                        try: self.registered_services.remove(service)
                        except ValueError: pass

                        try: self.outstanding_heartbeats.remove(service)
                        except ValueError: pass

                        self.logger.debug('Service %s unregistering.' % service)
                        confirmation = [service, S_SUCCESS]
                        self.services.send_multipart(confirmation)

                    # Heartbeat
                    elif signal == S_HEARTBEAT:
                        self.logger.debug(
                            'Received heartbeat from %s' % service)
                        self.outstanding_heartbeats.remove(service)

                elif len(msg) == 4:
                    service, client, signal, data = msg
                    self.logger.debug('Forwarding message (S->C):')
                    self.logger.debug('%s -> %s : \'%s\''
                    % (client, service, data))

                    self.clients.send_multipart([client, service, signal, data])

            # Client message
            if self.clients in incoming and incoming[
                                            self.clients] == zmq.POLLIN:
                msg = self.clients.recv_multipart()

                assert len(msg) == 4

                client, service, signal, data = msg

                if service not in self.registered_services:
                    self.logger.warning('Requested service not available.')
                    self.clients.send_multipart(
                        [client, service, S_UNAVAILABLE])

                self.logger.debug('Forwarding message (S->C):')
                self.logger.debug('%s -> %s : [%s] \'%s\''
                % (client, service, signal, data))
                self.services.send_multipart([service, client, signal, data])


        # Get here on SIGINT
        self.destroy()

    def _send_heartbeat_checks(self):
        for service in self.registered_services:
            self.logger.debug('Sending heartbeat request to %s.' % service)
            self.outstanding_heartbeats.append(service)
            self.services.send_multipart([service, S_HEARTBEAT])

    def destroy(self):
        """
        When this method is called, the broker unbinds from the client and
        service interfaces, closes the sockets and destroy it's internal ZMQ
        context.
        """
        self.logger.debug('Broker self-destructing.')
        self.services.close()
        self.clients.close()
        self.context.destroy()


class Service(pierre.PierreObject):
    """
    Service base class. A service is a process with a socket listening on the
    services interface for requests and maybe replying to requests it receives.

    :param identity: String identifier of the service. If this parameter is not
                     defined, the service will create a random identifier and
                     register with the broker using this random identifier. In
                     order to contact the service, this identifier must then be
                     retrieved manually. *Note: upon instantiation
                     a service will try to register with the broker using the
                     given identifier. If, however, a service has already
                     registered using the same identifier, the service will
                     self-destruct and raise a :py:exc:`RuntimError`.*

    :param service_addr: String address of the services interface address. If
                         this parameter is not set or set to None,
                         :py:data:`.DEFAULT_TIMEOUT` will be used.

    :param timeout: Polling timeout in milliseconds as an int or float. If not
                    set or None, :py:data:`.DEFAULT_TIMEOUT` will be used.

    .. seealso::

        SOA architecture description: :py:mod:`pierre.processing`

    """

    #: Timeout for registration with broker.
    REGISTRATION_TIMEOUT = 1500
    #: Timeout for unregistration with broker.
    UNREGISTRATION_TIMEOUT = 1500
    #: Number of tries before the service will give up trying to register with
    #: the broker.
    NUM_REGISTER_TRIES = 5
    #: Number of tries before the service will give up trying to unregister with
    #: the broker.
    NUM_UNREGISTER_TRIES = 3

    def __init__(self, identity=None, services_addr=None, timeout=None):
#        def _print(str_):
#            print(str_)
#
#        self.logger.debug = _print
#        self.logger.info = _print
#        self.logger.warning = _print
#        self.logger.error = _print

        #@TODO: None identity -> random identifier!
        self.identity = identity
        self.services_addr = services_addr or DEFAULT_SERVICES_ADDR
        self.timeout = timeout or DEFAULT_TIMEOUT

        self.context = zmq.Context()
        self.poller = zmq.Poller()

        self.broker = self.context.socket(zmq.DEALER)
        self.broker.setsockopt(zmq.IDENTITY, self.identity)
        self.broker.connect(self.services_addr)

        self.poller.register(self.broker, zmq.POLLIN)

        # Let the sockets settle before registering.
        time.sleep(1)

        self.register_with_broker()

    def register_with_broker(self):
        """
        Register the service with the broker.
        """

        self.logger.debug('Registering with broker...')
        self.broker.send(S_REGISTER)

        for t in xrange(self.NUM_REGISTER_TRIES):
            self.logger.debug('Waiting for registration confirmation '\
                              '(try %i).' % (t + 1))

            reply = self.poller.poll(self.REGISTRATION_TIMEOUT)

            if reply:
                resp_sig = self.broker.recv_multipart()
                self.logger.debug('Registration request reply signal: %s'
                % resp_sig)

                assert len(resp_sig) == 1
                resp_sig = resp_sig[0]

                self.logger.debug('Registration reply: %s' % resp_sig)

                if resp_sig == S_SUCCESS:
                    self.logger.debug('Registration successful.')
                    return

                elif resp_sig == S_IDENTTAKEN:
                    self.logger.error('Unable to register with broker, ' +
                                      'service with identical identity ' +
                                      'already registered.')

                    #@TODO
                    raise RuntimeError('Unable to register with broker, '+
                                       'service with identical identity ' +
                                       'already registered.')

        self.logger.error('Unable to register with broker.')
        #@TODO
        raise RuntimeError('Unable to register with broker.')


    def unregister_with_broker(self):
        """
        Unregister from the broker.

        .. note::
            
            Unregistration won't often work as the broker will often be killed
            before the service. This doesn't really matter. Unregistration is
            really only important if the service quits and the broker doesnt!

        """
        for t in xrange(self.NUM_UNREGISTER_TRIES):
            self.broker.send(S_UNREGISTER)

            reply = None
            try:
                reply = self.poller.poll(self.UNREGISTRATION_TIMEOUT)
            except zmq.ZMQError:
                pass

            if reply:
                resp_sig = self.broker.recv_multipart()
                assert len(resp_sig) == 1
                resp_sig = resp_sig[0]

                if resp_sig == S_SUCCESS:
                    self.logger.debug('Successfully unregistered from ' +
                                      'broker.')
                    return

            if t + 1 < self.NUM_UNREGISTER_TRIES:
                self.logger.warning('Unable to unregister from broker. ' +
                                    'Trying again...')

        self.logger.error('Unable to unregister from broker.')


    def serve(self):
        """
        Service loop. Here the service will serve until a KeyboardInterrupt or
        a S_SHUTDOWN signal is received.
        """

        def sigint(sig,frame):
            self.running = False
            self.logger.info('Received signal %i. Quitting!' % sig)

        signal_.signal(signal_.SIGINT,sigint)
        signal_.signal(signal_.SIGTERM,sigint)

        self.logger.info('Entering service loop...')

        self.running = True
        while self.running:
            try:
                incoming = self.poller.poll(self.timeout)
            except KeyboardInterrupt:
                raise
                #self.logger.info('Service \'%s\' shutting down.'
                #% self.identity)
                #break
            except zmq.ZMQError:
                pass

            if incoming:
                msg = self.broker.recv_multipart()
                assert len(msg) in (1, 3)

                # Signal message (see module-level documentation, a signaling
                # message is always exactly of length 1).
                if len(msg) == 1:
                    signal = msg.pop(0)
                    self.logger.debug('------------------------')
                    self.logger.debug('Received signal message:')
                    self.logger.debug('signal: %s' % signal)

                    self.handle_signal(signal)
                    # Signal handling may have changed the running status.
                    # This is how this function should be excited!
                    if not self.running:
                        return

                # Client request (see module-level documentation, a client
                # request is always exactly of length 3).
                elif len(msg) == 3:
                    client, signal, data = msg
                    self.logger.debug('------------------------')
                    self.logger.debug('Received client request:')
                    self.logger.debug('client: %s' % hexlify(client))
                    self.logger.debug('signal: %s' % signal)
                    self.logger.debug('data:   %s' % data)

                    self.handle(client=client, signal=signal, data=data)

        # Should never get here (but just in case)
        self.unregister_with_broker()
        self.destroy()

    def handle_signal(self, signal):
        """
        This method is responsible for handling signaling messages. It handles
        heartbeats and shutdown signals and should not be overriden.
        """
        # When a shutdown signal is received, the service must immediately
        # respond with a S_SUCCESS signal, unregister itself and finally
        # self-destruct.
        if signal == S_SHUTDOWN:
            self.logger.info('Received %s signal...shutting down.'
            % signal)
            self.broker.send(S_SUCCESS)
            self.unregister_with_broker()
            self.destroy()

        # When a heartbeat signal is received, the service must immediately
        # reply with a S_SUCCESS signal.
        elif signal == S_HEARTBEAT:
            self.logger.debug('Received heartbeat request. Replying...')
            self.broker.send(S_HEARTBEAT)

        # No idea how to handle anything else!
        else:
            self.logger.error('Received unknown signal \'%s\'.' % signal)

    def handle(self, client, signal, data):
        """
        The handle method is responsible for handling client requests. Any
        client request is forwarded to this method. The method should not return
        anything and should, if required reply to the client itself.

        :param client: String of the requesting client's identity.
        :param signal: Message signal (see module-level signaling documention).
        :param data: String of message data.
        """

        if   signal == S_MUSTREPLY:
            # Simply reverse the data string for now :)
            reply = data[::-1]
            self.broker.send_multipart([client, S_SUCCESS, reply])

        elif signal == S_NOTREPLY:
            # Do nothing for now.
            pass

        else:
            self.logger.error('Unknown client request signal \'%s\'.'
            % signal)
            self.broker.send_multipart([client, S_FAILURE, ''])

    def destroy(self):
        """
        Self-destruction function responsible for closing the socket and
        destroying the internal ZMQ context.
        """
        self.logger.debug('Service \'%s\' self-destructing.'
        % self.identity)
        self.broker.close()
        self.context.destroy(0)


class Client(pierre.PierreObject):
    """
    Client base class. The client is a service customer who can send requests
    to services and get responses from them.

    :param clients_addr: String with address of client interface. If this
                         parameter is not set or set to None, the module-level
                         DEFAULT_CLIENTS_ADDR value will be used.

    """

    def __init__(self, clients_addr=None,timeout=None):
        self.clients_addr = clients_addr or DEFAULT_CLIENTS_ADDR
        self.timeout = timeout or DEFAULT_TIMEOUT

        self.context = zmq.Context()
        self.poller  = zmq.Poller()

        self.sock = self.context.socket(zmq.DEALER)
        self.sock.connect(self.clients_addr)
        self.poller.register(self.sock, zmq.POLLIN)


    def destroy(self):
        """
        This method closes a client's socket and then destroys it's internal
        ZMQ context.
        """
        self.sock.close()
        self.context.destroy()

    def request(self,service,signal,data='',timeout=None,wait_reply=True):
        """
        Request method used to request service from a specific service.

        :param service: service identifier
        :type service: string

        :param signal: request signal
        :type signal: signal as defined in :py:mod:`pierre.processing`

        :param data: request data

        :param timeout: (option) milliseconds reply read timeout
        :type timeout: int

        :param wait_reply: whether or not the method should wait for and return
                           the service's reply. Note: if this parameter is set
                           to 'True', this method will block until the service
                           replies 
        :type wait_reply: bool

        .. warning::

            What happens when wait_reply is set, but another service replies
            to a former request, where a reply was not waited for? This
            method would then return the reply to the wrong request (think
            about whether or not this should even be allowed in a non-async
            client). A better idea might be to just immediately return if
            there is a guarantee that the service will not reply, otherwise
            block until the service reply. Also, if wait_reply is False, the
            client will not know whether or not the service it sent the
            request to actually exists.
        """

        timeout = timeout or self.timeout

        if signal not in SIGNALS:
            raise TypeError('Invalid signal \'%s\'!' % signal)

        msg = [service, signal, data]
        self.sock.send_multipart(msg)

        #if not wait_reply:
        #    return

        #reply = self.poller.poll(timeout)
        #if reply:
        #    reply = self.sock.recv_multipart()
        #    return reply
        #else:
        #    raise IPCReadTimeout()
        return self.sock.recv_multipart()
