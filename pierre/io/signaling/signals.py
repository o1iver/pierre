# -*- coding: utf-8 -*-
"""
    pierre.io.signaling.signals
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.

    TODO:
        - add strict/non-strict min/max for numeric types
"""

import time

TIME_FORMAT = '%Y-%m-%dT%H:%M:%S'
EPOCH_TIME = time.strftime(TIME_FORMAT,time.gmtime(0))

def current_time():
    return time.strftime(TIME_FORMAT)

class Signal(object):
    """
    Basic signal object. These objects are proxy objects attached to a signal
    processor. They do not hold the actual signal value (except for a start
    value) or timestamps. They are only responsible for value and type checking
    (meaning that they can check whether a value being written to it is within
    a certain range or choice of values and whether or not the value is of the
    correct type).

    :param alias: signal alias
    :type alias: string

    :param read_only: when set to True, will not allow signal writes (default:
                      False)
    :type read_only: bool

    :param processor: (optional) signal processor to attach the signal to
    :type processor: :py:class:`pierre.io.signaling.SignalProcessor`

    .. note:: 

        The signal class is closely linked to :py:class:`pierre.io.IOInterface`
        and changes should thus be made only when taking into consideration these
        effect may have on :py:class:`pierre.io.IOInterface` and more importantly
        on :py:class:`pierre.io.IOInterfaceMeta`.


    .. note::
        
        Signal contstructors must take **kwargs (see: 
        :py:class:`pierre.io.IOInterfaceMeta`).

    """
    def __init__(self,alias,read_only=False,processor=None,**kwargs):

        self.processor = processor

        self.alias = alias

        self.read_only = read_only

        self.first_read = True
        self.first_write = True
        if not hasattr(self,'start_value'):
            self.start_value = None

    def write(self,value):
        """
        The write function can be used to set the signal value. It calls the
        signal's :py:meth:`.Signal.valid_value` method, which may throw a
        :py:exc:`TypeError` or :py:exc:`ValueError` if the value does not comply
        to the signal rules. This method also checks whether or not the signal
        is defined as read-only, and will throw an exception if it is. If the 
        value has passed the :py:meth:`.Signal.valid_value` check, the signal's
        processor's :py:meth:`pierre.io.signaling.SignalProcessor.write` method
        will be called with the new signal value.

        :param value: signal value
        """
        #TODO: hack with "and not self.first_write"
        if self.read_only and not self.first_write:
            raise RuntimeError('Trying to write to read-only signal.')

        if self.valid_value(value):
            self.processor.write(self.alias,value)
            self.first_write = False
        else:
            # Exception should have been raised by valid_value
            raise RuntimeError('Shouldn\'t have gotten here! Does '+
                       'valid_value return True if valid (it must)?')

    def read(self):
        """
        The read function is responsible for providing the most recent signal
        value and timestamp to the signal instance's owner. It does this by
        calling it's signal processor's 'read' method.

        Because the controller logic may require a read before the first write,
        the signal will, on and only on the first read, provide the signal's
        start value if the signal processor throws an exception (as it should
        because it will in all likelihood not be able to provide a value either).
        This start value is also written to the processor in this case.

        :rtype: tuple(value, timestamp string in ISO8601 format)
        """
        try:
            value,timestamp = self.processor.read(self.alias)
        #@ TODO: fix this exception (should be able to distinguish
        #         between value not available and some other problem, such as
        #         not having a connection or something).
        except Exception as err:
            if self.first_read and self.first_write:
                self.value = self.start_value
                value = self.start_value
                timestamp = current_time()
                self.first_read = False
            else:
                raise err
        return (value,timestamp)

    def valid_value(self,value):
        """
        This method should be overridden for specialized signal types. It should
        return *True* if the given value is an acceptable signal value or *False*
        if the value is not acceptable.

        Usually this method should check the type and sometimes whether or not
        the value is within some permissible range. If the value type is not
        correct a :py:exc:`TypeError` should be raised, if the type is correct,
        but the value is not, a :py:exc:`ValueError` should be raised.

        .. note::

            In order to allow subclasses to ignore value checking, this method
            is implemented in the base class and returns 'True' by default.

        :param value: value
        :rtype: Bool (base class: True)
        """
        return True

    @property
    def timestamp(self):
        """
        The timestamp property returns the signal's last write date/time as an
        ISO8601 timestamp string (see format defined at module level).

        .. note::

            Reading this property entails a request to the signal processor,
            meaning that if a signal customer requires both the signal value
            and the last write time at the same time, it should directly
            use the read method (which would require only one call).

        :rtype: timestamp string in ISO8601 format
        """
        return self.read()[1]

    @property
    def value(self):
        """
        The value property returns a signal's most recent value as a native
        python object.

        .. note::

            Reading this property entails a request to the signal processor,
            meaning that if a signal customer requires both the signal value
            and the last write time at the same time, it should directly
            use the read method (which would require only one call).

        :rtype: value
        """
        return self.read()[0]

    @value.setter
    def value(self,value):
        """
        Value write property. Simple passes the value onto the 'write' method
        (see the documentation there).
        """
        self.write(value)

class IntegerSignal(Signal):
    """
    Integer signal.

    :param start_value: start value (default: 0)
    :type start_value: int
    :param min: minimum signal value (default: -inf)
    :type min: int

    :param max: maximum signal value (default: +inf)
    :type max: int

    :param read_only: when set to True, will not allow signal writes (default:
                      False)
    :type read_only: bool
    """
    def __init__(self,alias,start_value=None,min=None,max=None,read_only=False,
                 processor=None,**kwargs):
        super(IntegerSignal,self).__init__(alias=alias,read_only=read_only,
                processor=processor)
        self.min = min if min is not None else float('-inf')
        self.max = max if max is not None else float('+inf')
        self.start_value = start_value or 0

    def valid_value(self,value):
        """
        Checks whether the value is a valid value for an :py:class:`.IntegerSignal`:

        Conditions:

        - value of type int
        - value is greater-then-or-equal-to the signal's minimum value
        - value is smaller-then-or-equal-to the signal's maximum value

        :param value: value to check
        :rtype: bool
        """
        # Check type
        if not isinstance(value,int):
            raise TypeError('Value %s not of required type %s.'
            % (value,int.__name__))
            # Check if within range
        if not (self.min <= value <= self.max):
            min_str = str(self.min) if not isinstance(self.min,float)\
                                    else '-inf'
            max_str = str(self.max) if not isinstance(self.max,float)\
                                    else '+inf'
            raise ValueError('Value %i outside valid range (min: %s, '\
                             'max: %s).' % (value,min_str,max_str))
        return True


class UnsignedIntegerSignal(IntegerSignal):
    """
    Unsigned integer signal.

    :param start_value: start value (default: 0)
    :type start_value: unsigned int

    :param max: maximum signal value (default: +inf)
    :type max: int 

    :param read_only: when set to True, will not allow signal writes (default:
                      False)
    :type read_only: bool

    Note: zero ('0') is considered valid.
    """
    def __init__(self,alias,start_value=None,max=None,read_only=False,
            processor=None,**kwargs):
        super(UnsignedIntegerSignal,self).__init__(alias=alias,
            start_value=start_value,
            min=0,max=max,
            read_only=read_only,processor=processor)

class FloatSignal(Signal):
    """
    Float signal.

    :param start_value: start value (default: 0.0)
    :type start_value: float
    :param min: minimum signal value (default: -inf)
    :type min: float

    :param max: maximum signal value (default: +inf)
    :type max: float

    :param read_only: when set to True, will not allow signal writes (default:
                      False)
    :type read_only: bool

    Note: this signal does not do any casting (not even from integer to float).
    """
    def __init__(self,alias,start_value=None,min=None,max=None,read_only=False,
            processor=None,**kwargs):
        super(FloatSignal,self).__init__(alias=alias,read_only=read_only,
                processor=processor)
        self.min = min if min is not None else float('-inf')
        self.max = max if max is not None else float('+inf')
        self.start_value = start_value or 0.0

    def valid_value(self,value):
        """
        Checks whether the value is a valid value for a :py:class:`.FloatSignal`:

        Conditions:

        - value is of type float
        - value is greater-then-or-equal-to the signal's minimum value
        - value is smaller-then-or-equal-to the signal's maximum value

        :param value: value to check
        :rtype: bool
        """
        # Check type
        if not isinstance(value,float):
            raise TypeError('Value %s not of required type %s.'
            % (value,float.__name__))
            # Check if within range
        if not (self.min <= value <= self.max):
            raise ValueError('Value %i outside valid range (min: %f, '\
                             'max: %f).' % (value,self.min,self.max))
        return True

class StringSignal(Signal):
    """
    String signal.

    :param start_value: start value (default: '')
    :type start_value: string

    :param choices: valid signal values (if this parameter is not set or None,
                    any string signal will be considered valid)
    :type choices: list of strings

    :param read_only: when set to True, will not allow signal writes (default:
                      False)
    :type read_only: bool
    """
    def __init__(self,alias,start_value=None,choices=None,read_only=False,
            processor=None,**kwargs):
        super(StringSignal,self).__init__(alias=alias,read_only=read_only,
                processor=processor)
        self.choices = choices
        self.start_value = start_value or ''

    def valid_value(self,value):
        """
        Checks whether the value is a valid value for a :py:class:`.StringSignal`:

        Conditions:

        - value is of type str
        - value is in :py:attr:`.StringSignal.choices` (if choices were defined)

        :param value: value to check
        :rtype: bool
        """
        # Check type
        if not isinstance(value,str):
            raise TypeError('Value %s not of required type %s.'
            % (value,str.__name__))

        # Check choices
        if self.choices and value not in self.choices:
            raise ValueError('Value %s not in choices: %s.'
            % (value,self.choices))
        return True

class BooleanSignal(Signal):
    """
    Boolean signal.

    :param start_value: start value (default: True)
    :type start_value: bool

    :param read_only: when set to True, will not allow signal writes (default:
                      False)
    :type read_only: bool
    """
    def __init__(self,alias,start_value=None,read_only=False,processor=None,
            **kwargs):
        super(BooleanSignal,self).__init__(alias=alias,read_only=read_only,
                processor=processor)
        self.start_value = start_value or True

    def valid_value(self,value):
        """
        Checks whether the value is a valid value for a :py:class:`.BooleanSignal`

        Conditions:

        - value is of type bool

        :param value: value to check
        :rtype: bool
        """
        # Check type
        if not isinstance(value,bool):
            raise TypeError('Value %s not of required type %s.'
            % (value,bool.__name__))
        return True

