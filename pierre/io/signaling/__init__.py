# -*- coding: utf-8 -*-
"""
    pierre.io.signaling
    ~~~~~~~~~~~~~~~~~~~


    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""


import time

import json

# TODO: fix these imports here (shouldnt really be here!)
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import pierre
import pierre.processing as processing
import pierre.errors as errors

from .signals import Signal
from .signals import IntegerSignal
from .signals import UnsignedIntegerSignal
from .signals import FloatSignal
from .signals import StringSignal
from .signals import BooleanSignal

TIME_FORMAT = '%Y-%m-%dT%H:%M:%S'

class SignalReadError(errors.PierreIOError):
    """
    .. todo::

        - documentation

    """
    pass

class SignalWriteError(errors.PierreIOError):
    """
    .. todo::

        - documentation

    """
    pass

class SignalProcessor(pierre.PierreObject):
    """

    Basic signal processor interface. A signal processor must define a
    :py:meth:`.SignalProcessor.write` and a
    :py:meth:`.SignalProcessor.read` method.

    .. note::

        In reality a :py:class:`.SignalProcessor` will always be attached
        to a signal processing service that runs in a seperate process and
        can be used by multiple controllers' IO interfaces. If a signal
        processor is instantiated as a controller attribute it is useless
        because the to-be-controlled system cannot actually read or write
        signal values.

    :param listeners: List of signal listeners (for logging or evaluation).

    .. todo ::

        - listeners

     """
    def __init__(self,listeners=None):
        self.listeners = listeners or []

    def write(self,alias,value):
        """
          Given a signal alias and a value, this method must write this value to
          the signal.

          :param alias: signal alias.
          :type alias: string

          :param value: signal value.
          """
        raise NotImplementedError

    def read(self,alias):
        """
        Given a signal alias this function must return a two-tuple containing
        first the signal value (as a native python data type) and second the
        signal's last write-time as a datetime struct.

        :param alias: signal alias.
        :type alias: string
        :rtype: tuple(value,datetime)

        .. todo::

            Does it really return a datetime struct?


          """
        raise NotImplementedError


class BasicSignalProcessor(SignalProcessor):
    """
    This is the simples possible signal processor. It implements the
    :py:class:`.SignalProcessor` interface. Although it can be used locally,
    in most instances it will run within a :py:class:`.SignalProcessorService`
    process (so that multiple clients can connect to it).
    """
    def __init__(self,listeners=None,**kwargs):
        super(BasicSignalProcessor,self).__init__()
        self.listeners = listeners or []
        self.signal_values = {}

    def write(self,alias,value):
        now = time.strftime(TIME_FORMAT)
        self.signal_values[alias] = (value,now)
        self.logger.debug('Setting signal \'%s\' to \'%s\' (ts: %s)'
        % (alias,value,now))

    def read(self,alias):
        try:
            value,timestamp = self.signal_values[alias]
            self.logger.debug('Getting signal \'%s\': \'%s\' (ts: %s)'
            % (alias,value,timestamp))
            return value,timestamp
        except KeyError:
            raise SignalReadError('Signal %s does not exist!' % alias)

class PlottingSignalProcessor(BasicSignalProcessor):
    def __init__(self,listeners=None,plot_fnames=None,**kwargs):

        super(PlottingSignalProcessor,self).__init__(listeners,**kwargs)
        self.plot_fnames = plot_fnames or {}
        self.sig_val_hist = {}

    def add_sig_hist(self,alias,value=None):
        self.sig_val_hist[alias] = [value] if value else []

    def re_plot(self,alias):
        if alias not in self.plot_fnames:
            self.logger.debug('Not plotting signal \'%s\' as no plot filename'+
                    ' given.')
            self.logger.debug('Plotting signals: %s' % self.plot_fnames)
            return
        
        plot_fname = self.plot_fnames[alias]

        if alias not in self.sig_val_hist:
            self.logger.warning('Cannot plot signal \'%s\' as no history '\
                'available!' % alias)

        self.logger.debug('Redrawing plot for signal \'%s\'.' % alias)
        fig = plt.figure()
        p = fig.add_subplot(111)
        p.plot(self.sig_val_hist[alias])
        fig.suptitle('Signal: %s' % alias)
        fig.savefig(plot_fname)

    def write(self,alias,value):

        self.sig_val_hist.setdefault(alias,list())
        self.sig_val_hist[alias].append(value)

        self.re_plot(alias)

        BasicSignalProcessor.write(self,alias,value)



#: Helper function to create signal read request in the format used by the
#: py:class:`.SignalProcessorClient` and py:class:`.SignalProcessorService`
def _SignalReadRequest(alias):
    rq = {'rw': 'read',
          'alias': alias}
    return json.dumps(rq)

#: Helper function to create signal write request in the format used by the
#: py:class:`.SignalProcessorClient` and py:class:`.SignalProcessorService`
def _SignalWriteRequest(alias,value):
    rq = {'rw': 'write',
          'alias': alias,
          'value': value}
    return json.dumps(rq)

class SignalProcessorClient(pierre.PierreObject):
    """
    The :py:class:`.SignalProcessorClient` provides a client to be used by
    :py:class:`pierre.io.IOInterface` instances to communicate with signal
    processing services.

    .. note::

        This client is can be used manually, but it can also be defined in an
        IO interface YAML file as the *signal_processor* (see
        :py:mod:`pierre.io.generator`).

    .. seealso::

        :py:class:`.SignalProcessorService`

    """
    def __init__(self,identity,clients_addr=None,**kwargs):
        self.ipc_client = processing.Client(clients_addr=clients_addr)
        self.identity = identity

    def write(self,alias,value):
        """
        Given a signal alias and value, this method will contact the defined
        signal processing service and request that the signal value be overriden
        to the new value.

        :param alias: signal alias
        :type alias: string
        :param value: signal value
        """
        try:
            rp = self.ipc_client.request(self.identity,
                processing.S_MUSTREPLY,
                _SignalWriteRequest(alias,value))
            self.logger.debug('Got response: %s' % str(rp))
            self.logger.debug('Got reply to write: \'%s\'' % rp)
        except Exception as e:
            #@TODO
            raise e

        assert len(rp) in (2,3)

        # Got message from broker
        if len(rp) == 2:
            self.logger.debug('Got broker message...')
            _,signal = rp
            if signal == processing.S_UNAVAILABLE:
                #@TODO: exception type
                raise IOError('Unable to connect to service.')
            else:
                raise IOError('Received unsupported response signal %s'
                % signal)
        # Got message from service
        else:
            _,signal,_ = rp
            if   signal == processing.S_SUCCESS:
                return
            elif signal == processing.S_FAILURE:
                raise SignalWriteError('Unable to write signal %s.' % alias)
            else:
                raise IOError('Received unsupported response signal %s'
                % signal)


    def read(self,alias):
        """
        Given a signal alias, this method will try to contact the signal
        processing service to request a signal's value and last-write timestamp.

        :param alias: signal aliast
        :type alias: string
        :rtype: tuple(value,datetime string)
        """
        try:
            rp = self.ipc_client.request(self.identity,
                processing.S_MUSTREPLY,
                _SignalReadRequest(alias))
            self.logger.debug('Got response %s' % str(rp))
            self.logger.debug('Got reply to read: \'%s\'' % rp)
        except Exception as e:
            #@TODO
            raise e

        assert len(rp) in (2,3)

        # Got message from broker
        if len(rp) == 2:
            self.logger.debug('Got service message...')
            _,signal = rp
            if signal == processing.S_UNAVAILABLE:
                #@TODO: exception type
                raise IOError('Unable to connect to service.')
            else:
                raise IOError('Received unsupported response signal %s'
                % signal)

        # Got message from service
        else:
            _,signal,data = rp
            if   signal == processing.S_SUCCESS:
                pass
            elif signal == processing.S_FAILURE:
                raise SignalReadError('Unable to read signal %s.' % alias)
            else:
                raise IOError('Received unsupported response signal %s'
                % signal)

        data = json.loads(data)
        return (data['value'],data['timestamp'])

    def destroy(self):
        self.ipc_client.destroy()


class SignalProcessorService(processing.Service):
    """
    Basic signal processing service. This service should be started in the
    background and used as a centralized signal processor. It takes a signal
    processor instance and simply wraps that signal processor's read/write
    methods to support ZMQ-based IPC signaling IO.

    .. seealso::

        :py:class:`pierre.processing.Service`

    """
    def __init__(self, identity, signal_processor, services_addr=None,
                 timeout=None):
        super(SignalProcessorService, self).__init__(identity, services_addr,
            timeout)

        self.signal_processor = signal_processor

    def handle(self, client, signal, data):
        """
        This method overrides the base method of
        :py:class:`pierre.processing.Service` to provide IPC signal IO.

        :param client: client identifier
        :param signal: request signal
        :type signal: signal defined in :py:mod:`pierre.processing`
        :param data: request data

        .. seealso::

            :py:class:`.SignalProcessorClient`

        """
        def failure(err):
            self.broker.send_multipart([client, processing.S_FAILURE, err])
            return

        def success(data=''):
            self.broker.send_multipart([client, processing.S_SUCCESS, data])
            return

        data = json.loads(data)

        try:
            rw = data['rw']
        except KeyError:
            self.logger.error('Missing rw parameter!')
            failure('Missing rw parameter!')
            return

        try:
            alias = data['alias']
        except KeyError:
            self.logger.error('Missing signal alias!')
            failure('Missing signal alias!')
            return

        # Read signal value
        if rw == "read":
            try:
                value, timestamp = self.signal_processor.read(alias)
            except SignalReadError as e:
                self.logger.error('Error reading signal (err: %s)!' % str(e))
                failure('Error reading signal (err: %s)!' % str(e))
                return

            self.logger.debug('Read signal %s: %s' % (alias,str(value)))
            success(json.dumps({'value': value, 'timestamp': timestamp}))
            return

        # Write signal value
        elif rw == "write":
            try:
                value = data['value']
            except KeyError:
                failure('Missing signal value!')
                return

            try:
                self.logger.debug('Setting sig %s value to: %s'
                % (str(alias), str(data)))
                self.signal_processor.write(alias, value)
            except SignalWriteError as e:
                failure('Error writing signal (err: %s)!' % str(e))
                return

            success()
            return

        else:
            failure('rw parameter should be \'read\' or \'write\'')

class OPCSignalProcessor(pierre.PierreObject):
    def __init__(self):
        raise NotImplementedError()
