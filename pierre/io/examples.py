# -*- coding: utf-8 -*-
"""
    pierre.io.examples
    ~~~~~~~~~~~~~~~~~~


    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

from pierre.io import IOInterface

from pierre.io.signaling import UnsignedIntegerSignal
from pierre.io.signaling import StringSignal

from pierre.io.signaling import SignalProcessorClient

class RobotIOInterface(IOInterface):
    """
    Example robot controller interface.
    """

    signal_processor = SignalProcessorClient(identity='zmq_sig_processor_2')

    pos_x = UnsignedIntegerSignal(alias='orm.pos_x',max=9,read_only=True)
    pos_y = UnsignedIntegerSignal(alias='orm.pos_y',max=19,read_only=True)

    moves = ('Stay','North','NorthEast','East','SouthEast','South',
             'SouthWest','West','NorthWest')

    move  = StringSignal(alias='orm.move',start_value='Stay',choices=moves)

    def sense(self):
        last_move = self.move.value
        return RobotIOInterface.moves.index(last_move)

    def act(self,value):
        self.move.value = self.moves[value]

    def destroy(self):
        self.signal_processor.destroy()
