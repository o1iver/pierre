# -*- coding: utf-8 -*-
"""
    pierre.io.generator
    ~~~~~~~~~~~~~~~~~~~


    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.

    This module provides some rudimentary IO interface generation functionality.
    Defining custom subclasses of :py:class:`.IOInterface` for systems with
    large real and controller spaces is tedious and error prone. In order to
    facilitate this interface generation, the process has been automated. A
    relatively simple, human- and machine-readable YAML interface file must be
    produced, either by hand or automatically. This file is then taken by the
    :py:func:`.generate_interface` function, which produces a completely new
    :py:class:`.IOInterface` subclass.

    In the generated interface class, the generator defines a class-level signal
    processor (must be subclass of
    :py:class:`pierre.io.signaling.SignalProcessor`), zero or more class-level
    signal attributes (must be subclasses of
    :py:class:`pierre.io.signaling.signals.Signal`), a
    :py:meth:`.IOInterface.sense` method, which reads signal values and maps
    these to controller values, and finally a :py:meth:`.IOInterface.act`
    method, which maps controller values to real values and writes these to
    their respective signals

    The syntax of the interface file is:

    .. sourcecode:: none

          name: <interface_name>

          signal_processor:
            type: <signal_processor_class>
            parameters: # dict of parameters for <signal_processor_class> constructor
                identity: <processor_service_identity>
                <more_parameters>

          signals: # list of signal dicts
            -   name: <signal_name>
                type: <signal_class> # from pierre.io.signaling.signals
                parameters: # dict of parameters for <signal_class> constructor
                    alias: <signal_processing_alias>
                    <more_parameters>
            - <more_signals>

          states: # dict mapping controller values to real values predicates
            <controller_value>:
                name: <controller_value_name>
                predicates: # list of predicates used to test real_values
                    - "<signal_name> >= <some_value>"
                    - "<signal_name> < <some_value> <= <other_signal_name>"
                    - <more_predicates>
            <more_controller_values>

          actions: # dict mapping controller values to real values
            0:
                name: <action_name>
                values: # dict mapping signal to action value (which will be set)
                    <signal>: <desired_value>
                    <more_signals>

    Here is part of an example interface file, containing all of the currently
    supported syntax:

    .. sourcecode:: yaml

        # file: orm_interface.yaml

        name: ObstacleRobotInterface

        # Signal processor
        signal_processor:
            type: SignalProcessorClient
            params:
                identity: zmq_signal_processor

        # Signals
        signals:
            -   name: pos_x
                type: UnsignedIntegerSignal
                parameters:
                    alias: orm.pos_x
                    max: 9
                    read_only: True
            -   name: pos_y
                type: UnsignedIntegerSignal
                parameters:
                    alias: orm.pos_y
                    max: 19
                    read_only: True
            -   name: move
                type: StringSignal
                parameters:
                    alias: orm.move
                    start_value: 'DontMove'
                    choices: ['DontMove','North','NorthEast','East']

        # States
        states:
            0:
                name: grid_pos_0
                predicates:
                    - "pos_x == 0"
                    - "pos_y == 0"
            1:
                name: grid_pos_1
                predicates:
                    - "pos_x == 1"
                    - "pos_y == 0"
            2:
                name: grid_pos_2
                predicates:
                    - "pos_x == 2"
                    - "pos_y == 0"
            3:
                name: grid_pos_3
                predicates:
                    - "pos_x == 3"
                    - "pos_y == 0"
            4:
                name: grid_pos_4
                predicates:
                    - "pos_x == 4"
                    - "pos_y == 0"
        # Actions:
        actions:
            0:
                name: Stay
                values:
                    move: Stay
            1:
                name: North
                values:
                    move: North
            2:
                name: NorthEast
                values:
                    move: NorthEast
            3:
                name: East
                values:
                    move: East

    An interface class could be generated from the above file using the
    'generate_interface' function:

    .. sourcecode:: python

        ObstacleRobotInterface = generate_interface('orm_interface.yaml')

    If the produced interface class should also be saved to file (useful for
    debugging), a second parameter should be passed:

    .. sourcecode:: python

        ObstacleRobotInterface = generate_interface('orm_interface.yaml','orm_interface.py')

    .. note::

        This interface generator is very sensitive and will easily crash
        (without helpful error messages). In that case make sure that the
        interface YAML file is correctly formatted.

    .. todo::

        - Change 'states' to 'controller_values'.
        - Produce a real grammar file


"""

import jinja2

from pierre.io import IOInterface

from pierre.io.signaling import *
3

import time
import yaml
import jinja2

# Jinja IOInterface template
io_interface_template = """
# Autogenerated IOInterface
# generated: {{generation_timestamp}}

from pierre.io import IOInterface

from pierre.io.signaling import *

class {{name}}(IOInterface):

    # Signal processor
    signal_processor = {{signal_processor.type}}({% for name,value in signal_processor.parameters.iteritems() %}{{name}}={{value|str if value is str else value}},{% endfor %})

    # Signals{% for signal in signals %}
    {{signal.name}} = {{signal.type}}({% for name,value in signal.parameters.iteritems() %}{{name}}={{value|str if value is str else value}},{% endfor %}){% endfor %}

    # Sense: convert real values to controller values
    def sense(self):

        # Signal Values
        {% for signal in signals %}{{signal.name}} = self.{{signal.name}}.value
        {% endfor %}
        {% for index,state in states.iteritems() %}
        # Index: {{index}}
        # Name:  {{state.name}}
        {{ 'if ' if loop.first else 'elif' }} {% for predicate in state.predicates %}({{predicate}}){{' and ' if not loop.last}}{% endfor %}:
            return {{index}}
        {% endfor %}

    # Act: convert controller values to real values (signal values)
    def act(self,controller_value):
        {% for controller_value,action in actions.iteritems() %}
        # Value: {{controller_value}}
        # Name:  {{action.name}}
        {{ 'if ' if loop.first else 'elif' }} controller_value == {{controller_value}}:
            {% for signal_name,signal_value in action['values'].iteritems() %} self.{{signal_name}}.value = {{ signal_value|str if signal_value is str else signal_value}}
            {% endfor %}{% endfor %}

"""


def _read_file(fname):
    with open(fname,'r') as f:
        return f.read()

def _write_file(fname,str_):
    with open(fname,'w') as f:
        f.write(str_)

def _read_yaml(fname):
    return yaml.load(_read_file(fname))

# Jinja test if it string
def _is_str(str_):
    return isinstance(str_,str)

# Jinja filter to quote strings
def _to_str(str_):
    return "'" + str_ + "'"

def generate_interface(yaml_interface_fname,out_fname=None):
    """
    This function takes the path to an IO interface YAML file and returns a
    generated interface class. Additionally it can write the generated IO
    interface to file.

    :param yaml_interface_fname: path to the IO interface YAML file
    :type yaml_interface_fname: string
    :param out_fname: (optional) path to file the new interface class should be
                      written to
    :type out_fname: string
    :rtype: :py:class:`pierre.io.IOInterface` subclass
    """

    jinja_loader = jinja2.DictLoader({'io_interface': io_interface_template})
    jinja_env = jinja2.Environment(loader=jinja_loader)
    jinja_env.tests['str'] = _is_str
    jinja_env.filters['str'] = _to_str

    dct = _read_yaml(yaml_interface_fname)
    dct['generation_timestamp'] = time.strftime('%Y-%m-%dT%H:%M:%S')

    t = jinja_env.get_template('io_interface')

    interface = t.render(**dct)

    if out_fname:
        _write_file(out_fname,interface)

    exec interface
    return eval(dct['name'])



