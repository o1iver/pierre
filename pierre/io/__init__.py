# -*- coding: utf-8 -*-
"""
    pierre.io
    ~~~~~~~~~


    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

import json

import pierre

from .signaling import Signal
from .signaling import IntegerSignal
from .signaling import UnsignedIntegerSignal
from .signaling import FloatSignal
from .signaling import StringSignal
from .signaling import BooleanSignal

class IOInterfaceMeta(type):
    """
    IOInterface metaclass. The idea of this metaclass is to facilitate the
    definition of a custom :py:class:`.IOInterface`. This is done by
    allowing the user to define a signal processor
    (:py:class:`pierre.io.signaling.SignalProcessor`) and signals
    (:py:class:`pierre.io.signaling.signals.Signal`) at the class
    level. The metaclass is necessary because even though the processor and the
    signals are defined as class attributes, they should be instance attributes
    (otherwise two controllers using interface instances from the same class
    would overwrite each others' signals).

    See the comments in the :py:meth:`pierre.io.IOInterfaceMeta.__new__` method
    for more details, but in short what happens here, is that before class
    creation (so at import time), a new dynamic constructor is created where
    the processor and signals defined as class variables are actually
    instantiated as instance variables.

    .. note::

        Because of this metaclass, all signal constructors must take **kwargs.

    .. note::

        Because of this metaclass all signal processor constructors must also
        take **kwargs.
    """
    def __new__(cls,name,bases,dct):
        # Break early if this is the IOInterface class
        if name == 'IOInterface':
            return super(IOInterfaceMeta,cls).__new__(cls,name,bases,dct)
        # Break early if the class has an attribute __ABSTRACT set to True (used
        # for abstract subclasses of IOInterface)
        elif dct.get('_'+name+'__ABSTRACT',False):
            return super(IOInterfaceMeta,cls).__new__(cls,name,bases,dct)

        # Get defined signals
        signals = dict((k,v) for k,v in dct.iteritems()
            if isinstance(v,Signal))

        # Get pre-defined init. If it doesn't exist it is set to None here and
        # then not called in the new init.
        old_init = dct.get('__init__',None)

        # Get the defined signal processor
        try:
            old_sig_processor = dct['signal_processor']
        except KeyError:
            raise RuntimeError('No signal processor defined in io ' +
                               'interface class %s' % name)

        # Get class of static processor instance. It is better to get the class
        # here to that the instance can be deleted and only the class needs to
        # remain in scope for use in the new init.
        old_sig_processor_class = old_sig_processor.__class__

        # Same as with the sig processor's class above, it's dict is extracted
        # here so that the instance itself can be garbage collected and does
        # not need to remain in scope for the new init method.
        old_sig_processor_dict  = old_sig_processor.__dict__

        # Delete unneeded static processor instance
        del old_sig_processor

        def new_init(obj,*args,**kwargs):
            # Create a new instance of the signal processor (note that the
            # creation of an instance here does not mean that a same processing
            # service cannot handle multiple interfaces' signals, it just means
            # that this proxy functionality must be handled in the processors
            # init function).
            sig_processor = old_sig_processor_class(**old_sig_processor_dict)

            # Loop through every statically defined signal
            for name,signal in signals.iteritems():
                # Create a new instance of the signal
                new_sig = signal.__class__(**signal.__dict__)
                # Delete the old signal instance
                del signal
                # Set the signal's processor (this processor is the same for all
                # signals)
                setattr(new_sig,'processor',sig_processor)
                # Set the actual signal on the object (the interface).
                setattr(obj,name,new_sig)

            # If an init function was actually defined for the io interface,
            # call that now.
            if old_init:
                old_init(obj,*args,**kwargs)
            # Otherwise just call super (@TODO: danger here using super!)
            else:
                super(obj.__class__,obj).__init__(*args,**kwargs)

        # Attach the new init function to the class definition.
        dct['__init__'] = new_init
        return super(IOInterfaceMeta,cls).__new__(cls,name,bases,dct)

class IOInterface(pierre.PierreObject):
    """
    IO interfaces are controller attributes that are responsible for firstly
    translating variables in control-space to values in real space and vice
    versa. Secondly IO interfaces are also responsible for communication
    between the controller and a signal processor (which then in turn
    transfers signal values to the *real world*).
    """

    __metaclass__ = IOInterfaceMeta

    def __init__(self):
        pass

    def act(self,*args,**kwargs):
        """
        Method that the controller calls when it wants to act on the controlled
        entity.
        """
        raise NotImplementedError

    def sense(self):
        """
        Method that the controller calls when it wants to receive an sensation
        about the observed system.
        """
        raise NotImplementedError

class AliasPrefixIOInterface(IOInterface):
    """
    IOInterface subclass that allows for global signal alias prefixing. Given a
    prefix string, this io interface will prefix all it's signals' alias with
    said prefix. This is useful when multiple controllers use the same interface,
    as it avoids having to manually provide unique signal aliases.

    :param prefix: prefix
    :type prefix: string
    :param ignore_read_only_signals: (default: False) do not change prefix of
                                     read-only signals
    :type ignore_read_only_signals: bool
    """

    #: This class attributes signals to the IOInterfaceMeta class, that it
    #: should ignore this class.
    __ABSTRACT = True

    def __init__(self,prefix='',ignore_read_only_signals=False):
        super(AliasPrefixIOInterface,self).__init__()
        self.add_prefixes(prefix,ignore_read_only_signals)

    def add_prefixes(self,prefix,ignore_read_only_signals):
        for k,v in self.__dict__.items():
            if isinstance(v,pierre.io.signaling.signals.Signal):
                if ignore_read_only_signals and v.read_only:
                    self.logger.debug('Not changing alias of read-only signal '+
                            '\'%s\'.' % v)
                    pass
                else:
                    self.logger.debug('Changing alias \'%s\' to \'%s.\''
                            % (v.alias, prefix+v.alias))
                    v.alias = prefix + v.alias

class WriteAliasPrefixIOInterface(AliasPrefixIOInterface):
    """
    AliasPrefixIOInterface subclass that simply removes the need of setting
    ignore_read_only_signals in the parent class to True manually, ie. this type
    of IOInterface will automatically prefix all signals that are not read-only.
    """

    #: This class attributes signals to the IOInterfaceMeta class, that it
    #: should ignore this class.
    __ABSTRACT = True

    def __init__(self,prefix=''):
        super(WriteAliasPrefixIOInterface,self).__init__(prefix=prefix,
                ignore_read_only_signals=True)
