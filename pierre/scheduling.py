# -*- coding: utf-8 -*-
"""
    pierre.scheduling
    ~~~~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

import time
import sched

import pierre

def epoch_milli_secs():
    """
    Returns the current time in milliseconds since the UNIX epoch.

    :rtype: int
    """
    return int(round(time.time()*1000))

class Scheduler(pierre.PierreObject):
    """
    This class provides a very simple scheduling interface. Events can be
    registered with a callback function that will be called whenever the event
    is triggered. The scheduler can also be passed a sleep time. This defines
    the number of milliseconds that the scheduler process will sleep between
    checking for possibly due events. The sleep time should be chosen as large
    as possible in order to save CPU time. If no sleep time is defined, the
    scheduler will use a default sleep time of 100 milliseconds.

    :param sleep_time: (optinal) sleep time in milliseconds. If not defined the
                       sleep time is set to 100 milliseconds.
    :type sleep_time: int
    """

    #_LOOPFUNC = "scheduler"
    _LOOPFUNC = "manual"

    _DEFAULT_SLEEP_TIME = 100

    def __init__(self,sleep_time=None):
        self.events = []
        self.sleep_time = sleep_time or Scheduler._DEFAULT_SLEEP_TIME

    def loop(self):
        """
        Main scheduling loop. The scheduler checks whether any of the
        registered events are due and calls their associated callbacks if they
        are. The scheduler sleeps a certain amount of seconds between each check.
        """
        self._set_event_start_times()

        self.logger.info("Starting scheduler loop.")
        if not self.events:
            #TODO: do something here
            self.logger.warning("No events defined.")
            pass
        try:
            if Scheduler._LOOPFUNC == "scheduler":
                self._scheduler_loop()
            elif Scheduler._LOOPFUNC == "manual":
                self._manual_loop()
        except KeyboardInterrupt:
            #TODO: clean this up
            raise

    def _set_event_start_times(self):
        """
        This function synchronizes the start times of all events.

        @TODO: change this, it's an ugly hack that only works for the events
               currently defined!
        """
        now = epoch_milli_secs()
        for (event,_,_,_) in self.events:
            event.start = now
            event.last  = now


    def _scheduler_loop(self):
        sched_ = sched.scheduler(time.time,time.sleep)
        while True:
            sched_.enter(self.sleep_time/1000,1,self._check_events,())
            sched_.run()

    def _manual_loop(self):
        while True:
            time.sleep(self.sleep_time/1000)
            self._check_events()

    def _check_events(self):
        """
        Triggers events' callback functions if they are due.
        """
        now = epoch_milli_secs()
        for (event,callback,args,kwargs) in self.events:
            if event.due(time_=now):
                callback(*args,**kwargs)

    def attach(self,event,callback,args=(),kwargs={}):
        """
        Attach an event and an associated callback to the scheduler.

        :param event: event to attach.
        :type event: :py:class:`.Event`
        :param callback: callback function or method.
        :param args: (not implemented yet)
        :param kwargs: (not implemented yet)
        """
        self.logger.debug("Attaching event: %s." % event)
        self.events.append((event,callback,args,kwargs))

    def detach(self,event):
        """
        Detach an event from the scheduler.

        :param event: event to detach.
        :type event: :py:class:`.Event`
        """
        self.logger.debug("Detaching event: %s" % event)
        event_,callback,args,kwargs = (None,None,None,None)
        for (event_,callback,args,kwargs) in self.events:
            if event_ == event:
                break
            else: event_ = None
        if event_:
            self.events.remove((event_,callback,args,kwargs))

class Event(pierre.PierreObject):
    """
    Event base type. An event is an absolute or relative definition of a point
    in time.
    """
    def __init__(self):
        # Is this event time based (which type of events wouldn't be?)
        self.time_based = None
        # The maximum time gap (in milliseconds) that the scheduler may sleep
        # before rechecking the event's due status.
        self.check_time = None


    def due(self):
        """
        This function must return a boolean signifying whether or not the
        event call is due.

        :rtype: bool
        """
        raise NotImplementedError()

class EveryNMilliSeconds(Event):
    """
    An event that triggers every N milliseconds.

    :param n_milli_secs: number of milliseconds between triggers.
    :type n_milli_secs: int
    """
    def __init__(self,n_milli_secs):
        self.time_based = True
        self.start = None
        self.msecs = n_milli_secs
        self.check_time = n_milli_secs
        self.last = None
        self.missed = 0

    def due(self,time_=None):
        """
        Return True if the event is due.

        :param time: (optional) current time in milliseconds since the epoch.
        :type time: int
        :rtype: bool
        """
        time_ = time_ or epoch_milli_secs()
        diff = time_ - self.last
        if diff >= self.msecs:
            self.last += self.msecs
            if diff > self.msecs:
                self.missed += (diff/self.msecs)
                # TODO: this misses stuff doesn't work exactly (probably because
                #       calculates missed milliseconds and not real misses!)
                #self.logger.debug("MISSED! Total missed: %s" % self.missed)
            return True
        return False

class EveryNSeconds(EveryNMilliSeconds):
    """
    An event that triggers every N seconds.

    :param n_seconds: number of seconds between triggers.
    :type n_seconds: int
    """
    def __init__(self,n_seconds):
        EveryNMilliSeconds.__init__(self,1000*n_seconds)

    def __unicode__(self):
        return u"[Event: every %s second(s)]" % (self.msecs/1000)

class EveryNMinutes(EveryNMilliSeconds):
    """
    An event that triggers every N minutes.

    :param n_minutes: number of minuted between triggers.
    :type n_minutes: int
    """
    def __init__(self,n_minutes):
        EveryNMilliSeconds.__init__(self,1000*60*n_minutes)

    def __unicode__(self):
        return u"[Event: every %s minute(s)]" % (self.msecs/(60*1000))

class EveryNHours(EveryNMilliSeconds):
    """
    An event that triggers every N hours.

    :param n_hours: number of hours between triggers.
    :type n_hours: int
    """
    def __init__(self,n_hours):
        EveryNMilliSeconds.__init__(self,1000*60*60*n_hours)

    def __unicode__(self):
        return u"[Event: every %s hour(s)]" % (self.msecs/(60*60*1000))

class EveryNDays(EveryNMilliSeconds):
    """
    An event that triggers every N days.

    :param n_days: number of days between triggers.
    :type n_days: int
    """
    def __init__(self,n_days):
        EveryNMilliSeconds.__init__(self,1000*24*60*60*n_days)

    def __unicode__(self):
        return u"[Event: every %s day(s)]" % (self.msecs/(24*60*60*1000))



