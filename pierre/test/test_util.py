# -*- coding: utf-8 -*-
"""
    pierre.test.test_util
    ~~~~~~~~~~~~~~~~~~~~~

    :copyright: (c) 2012 by Bastian Migge, Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

import unittest

import pierre.util as util

class TestInitialize(unittest.TestCase):
    def setUp(self):
        self.stubObj = type('Stub',(object,),{})()

    def test_initialize(self):
        kw = {"a":10,"b":True,"c":lambda x:x+x}
        util.initialize(self.stubObj,**kw)

        self.assertEqual(self.stubObj.a,10)
        self.assertEqual(self.stubObj.b,True)
        self.assertEqual(self.stubObj.c(2),4)

class TestInIter(unittest.TestCase):
    def setUp(self):
        stubClass = type('Stub',(object,),{})
        self.objs = []
        for i in range(10): self.objs.append(stubClass())
        self.testObjIn = stubClass()
        self.objs.append(self.testObjIn)
        self.testObjNotIn = stubClass()
        for i in range(10): self.objs.append(stubClass())

    def test_inIter(self):
        self.assertTrue(util.inIter(self.objs,self.testObjIn))
        self.assertFalse(util.inIter(self.objs,self.testObjNotIn))


class TestNumInIter(unittest.TestCase):
    def setUp(self):
        stubClass = type('Stub',(object,),{})
        self.objs = []
        self.numIn = 3
        for i in range(10): self.objs.append(stubClass())
        self.testObj = stubClass()
        for i in range(self.numIn):
            self.objs.append(self.testObj)
        for i in range(10): self.objs.append(stubClass())

    def test_numInIter(self):
        self.assertEqual(util.numInIter(self.objs,self.testObj),self.numIn)

