import os
import pierre.processing

DEFAULT_CONFIG = {
        'PIERRE_SERVICES_ADDR': None,
        'PIERRE_CLIENTS_ADDR': None,
        'PIERRE_HEARTBEAT_TIME': None,
        'PIERRE_TIMEMOUT': None }

config = dict(map(lambda x: (x[0],os.getenv(x[0],x[1])),DEFAULT_CONFIG.items()))

print("Configuration:")
print(config)

broker = pierre.processing.Broker(
        services_addr=config['PIERRE_SERVICES_ADDR'],
        clients_addr=config['PIERRE_CLIENTS_ADDR'],
        heartbeat_time=config['PIERRE_HEARTBEAT_TIME'],
        timeout=config['PIERRE_TIMEMOUT'])

broker.serve()


